<?php

namespace App\Http\Controllers;

use App\jurusan;
use Illuminate\Http\Request;

class JurusanController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        $jurusan = Jurusan::all();
        return view('jurusan.index', compact('jurusan'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
        return view('jurusan.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'nama_jurusan' => 'required',
            'keterangan' => 'required',
        ]);

        Jurusan::create($request->all());
        return redirect()->route('jurusan.index')
                         ->with('success', 'jurusan berhasil di simpan');
    }

    /**
     * Display the specified resource.
     */
    public function show(Jurusan $jurusan)
    {
        //
        return view('jurusan.edit', compact('jurusan'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Jurusan $jurusan)
    {
        //
        return view('jurusan.edit', compact('jurusan'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Jurusan $jurusan)
    {
        //
        $request->validate([
            'nama_jurusan' => 'required',
            'keterangan' => 'required',
        ]);

        $jurusan->update($request->all());

        return redirect()->route('jurusan.index')->with('success','Jurusan Done');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Jurusan $jurusan)
    {
        //
        $jurusan->delete();
        return redirect()->route('jurusan.index')->with('success','Jurusan berhasil di delete');
    }
}
