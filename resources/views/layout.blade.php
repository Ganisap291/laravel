<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.boostrapcdn.com/boostrap/4.5.1/css/boostrap.min.css">
</head>
<body>
    <div class="container">
        @yeld('content')
    </div>
</body>
</html>